import { RubricItem } from "./domain/rubric-item";

const fs = require('fs');

export const PATH_GRADES_CSV = './includes/grades.csv'
export const PATH_SECRET_TKN = './includes/token.secret'
export const SECRET = fs.readFileSync(PATH_SECRET_TKN)

export const RUN_AS_DEBUG = true;
export const IS_INDIVIDUAL_ASSIGNMENT = false;

export const COURSE_URL = 'https://canvas.wisc.edu/api/v1'
export const COURSE_ID = '83960000000271491'
export const ASSIGNMENT_ID = '83960000001380042'

export const CSV_ID_COL = 0;
export const CSV_GRADE_START = 1;
