import { ASSIGNMENT_ID, COURSE_ID, COURSE_URL, CSV_GRADE_START, CSV_ID_COL, IS_INDIVIDUAL_ASSIGNMENT, PATH_GRADES_CSV, RUN_AS_DEBUG, SECRET } from '../config';
import { Grade } from '../domain/grade';
import { GradedStudent } from '../domain/graded-student';
import { Group } from '../domain/group';
import { RubricItem } from '../domain/rubric-item';
import { Student } from '../domain/student';
import { wait } from './util'

const nfetchsync = require('sync-fetch')
const nfetch = require('node-fetch');
const fs = require('fs');
const CsvReadableStream = require('csv-reader');
var url = require('url');

let rubric_items: RubricItem[] = [];

export function fetchRubricIds(): any {
    let res = nfetchsync(`${COURSE_URL}/courses/${COURSE_ID}/assignments/${ASSIGNMENT_ID}`, {
        method: 'GET',
        headers: {
            'Authorization': 'Bearer ' + SECRET
        }
    }).json()
    
    res.rubric.forEach((rub: any) => rubric_items.push(new RubricItem(rub.id)));
}

export function fetchSubmissions(): any {
    let courseUrl = `${COURSE_URL}/courses/${COURSE_ID}/assignments/${ASSIGNMENT_ID}/submissions/?include[]=group&include[]=user&per_page=100`;
    let submissions: any[] = []
    console.log(`Beginning download...`)
    nfetch(courseUrl + '&page=1',
        {
            method: 'GET',
            headers: {
                'Authorization': 'Bearer ' + SECRET
            }
        })
        .then((res: any) => res.json())
        .then((res: any[]) => {
            console.log(`First page downloaded...`)
            submissions = submissions.concat(res);
            nfetch(courseUrl + '&page=2',
                {
                    method: 'GET',
                    headers: {
                        'Authorization': 'Bearer ' + SECRET
                    }
                })
                .then((res: any) => res.json())
                .then((res: any[]) => {
                    submissions = submissions.concat(res);
                    console.log(`Second page downloaded! Processing ${submissions.length} submissions!`);
                    if(IS_INDIVIDUAL_ASSIGNMENT) {
                        processSubmissionsIndividual(submissions);
                    } else {
                        processSubmissionsGroup(submissions);

                    }
                })
        })
}

export function processSubmissionsGroup(submissions: any[]) {
    let students: Array<Student> = [];
    submissions.forEach(submission => {
        if(submission && submission.group && submission.group.name) {
            let fullGroupName= submission.group.name
            let groupNum = fullGroupName.substr(fullGroupName.lastIndexOf(' ') + 1)
            let student = new Student(submission.user.id, submission.user.name, groupNum);
            students.push(student);
        } else {
            console.error(`Could not find a group for ${submission.user.name}. Ignoring!`)
        }
    });

    console.log(`Found ${students.length} students!`);
    students.forEach(student => console.log(student.toString()));
    let groups: Group[] = Group.asGroups(students);
    console.log(`Found ${groups.length} groups!`);
    groups.forEach(group => console.log(group.toString()));
    console.log("Beginning grade processing...");
    let gradedStudents: GradedStudent[] = [];
    let inputStream = fs.createReadStream(PATH_GRADES_CSV, 'utf8');
    inputStream
        .pipe(new CsvReadableStream({ parseNumbers: true, trim: true }))
        .on('data', function (row: any) {
            groups.forEach(group => {
                if(group.id === row[CSV_ID_COL].toString()) {
                    let grades: Grade[] = [];
                    rubric_items.forEach((rubricItem: RubricItem, i: number) => {
                        let startIndex = CSV_GRADE_START + (2 * i);
                        let grade = row[startIndex];
                        let comment = row[startIndex + 1];
                        grades.push(new Grade(rubricItem, comment, grade));
                    });
                    group.students.forEach(student => {
                        gradedStudents.push(new GradedStudent(student, grades));
                    });
                }
            })
        })
        .on('end', function (data: any) {
            console.log('End of CSV!');
            submitGrades(gradedStudents);
        });
}

function processSubmissionsIndividual(submissions: any[]) {
    let students: Array<Student> = [];
    submissions.forEach(submission => {
        if(submission) {
            let student = new Student(submission.user.id, submission.user.name, "none");
            students.push(student);
        } else {
            console.error(`Could not find a submission for ${submission.user.name}. Ignoring!`)
        }
    });
    console.log(`Found ${students.length} students!`);
    students.forEach(student => console.log(student.toString()));
    console.log("Beginning grade processing...");
    let gradedStudents: GradedStudent[] = [];
    let inputStream = fs.createReadStream(PATH_GRADES_CSV, 'utf8');
    inputStream
        .pipe(new CsvReadableStream({ parseNumbers: true, trim: true }))
        .on('data', function (row: any) {
            students.forEach(student => {
                if(student.name.toLowerCase() === row[CSV_ID_COL].toString().toLowerCase()) {
                    let grades: Grade[] = [];
                    rubric_items.forEach((rubricItem: RubricItem, i: number) => {
                        let startIndex = CSV_GRADE_START + (2 * i);
                        let grade = row[startIndex];
                        let comment = row[startIndex + 1];
                        grades.push(new Grade(rubricItem, comment, grade));
                    });
                    gradedStudents.push(new GradedStudent(student, grades));
                }
            })
        })
        .on('end', function (data: any) {
            console.log('End of CSV!');
            submitGrades(gradedStudents);
        });
}

export async function submitGrades(gradedStudents: GradedStudent[]) {
    console.log(`Grading complete! ${gradedStudents.length} students graded.`);

    /** BEGIN POINT OF NO RETURN - SUBMIT GRADES **/

    if (RUN_AS_DEBUG) {
        console.log('Running as debug. Grades are NOT submitted.');
    } else {
        console.log('Running as prod. Grades submitting in 10 seconds... Ctrl+C to cancel.');
        wait(10);
        for(let gradedStudent of gradedStudents) {
            console.log(`Grade for ${gradedStudent.student.name} en route!`);
    
            await nfetch(gradedStudent.asURL(),
            {
                method: 'PUT',
                headers: {
                    'Authorization': 'Bearer ' + SECRET
                }
            })
            .then((res: any) => {
                console.log(`Recieved HTTP ${res.status} for ${gradedStudent.student.name}`);
                return res.json()
            })
            .then((res: any[]) => { });
    
            // Stop Canvas Police, assume min wait time of 0.5 secs and add random offset. 🕵️
            wait(0.5 + (Math.random() * 0.1));
        }
    }
}