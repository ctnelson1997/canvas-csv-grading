// https://stackoverflow.com/questions/14249506/how-can-i-wait-in-node-js-javascript-l-need-to-pause-for-a-period-of-time
export function wait(seconds: number) {
    let waitTill = new Date(new Date().getTime() + seconds * 1000);
    while(waitTill > new Date()){}
}
