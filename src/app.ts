import { fetchSubmissions, fetchRubricIds } from './process/canvas'

fetchRubricIds();
fetchSubmissions();
