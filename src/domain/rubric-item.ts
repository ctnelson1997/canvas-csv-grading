export class RubricItem {
    public rubricId: string;

    public constructor(rubricId: string) {
        this.rubricId = rubricId;
    }

    public toString(): string {
        return `RID ${this.rubricId}`;
    }
}