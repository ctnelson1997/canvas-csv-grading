import { RubricItem } from "./rubric-item";

export class Grade {
    public rubricItem: RubricItem;
    public comment: string;
    public grade: number;

    public constructor(rubricItem: RubricItem, comment: string, grade: number) {
        this.rubricItem = rubricItem;
        this.comment = comment;
        this.grade = grade;
    }

    public toString(): string {
        return `${this.rubricItem.toString()}: ${this.grade} - ${this.comment}`;
    }
}