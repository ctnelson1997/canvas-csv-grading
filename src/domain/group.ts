import { Student } from './student'

export class Group {
    public id: string;
    public students: Student[];

    public constructor(id: string, students: Student[]) {
        this.id = id;
        this.students = students;
    }

    public static asGroups(students: Student[]): Group[] {
        let groups: Array<Group> = [];
        let assignments = new Map<string, Student[]>();
        students.forEach(student => {
            if(!assignments.has(student.group)) {
                assignments.set(student.group, []);
            }
            assignments.get(student.group)?.push(student);
        });
        assignments.forEach((students: Student[], groupNum: string, map) => {
            groups.push(new Group(groupNum, students));
        });
        return groups;
    }

    public toString(): string {
        let ret = `G${this.id}`;
        this.students.forEach(student => ret += `\n - ${student.toString()}`);
        return ret;
    }
}