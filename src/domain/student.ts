export class Student {
    public id: number;
    public name: string;
    public group: string;

    public constructor(id: number, name: string, group: string) {
        this.id = id;
        this.name = name;
        this.group = group;
    }

    public toString(): string {
        return `G${this.group} U ${this.id} - ${this.name}`;
    }
}