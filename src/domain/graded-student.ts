import { Student } from './student'
import { Grade } from './grade'
import { COURSE_URL, COURSE_ID, ASSIGNMENT_ID } from '../config';

export class GradedStudent {
    public student: Student;
    public grades: Grade[];

    public constructor(student: Student, grades: Grade[]) {
        this.student = student;
        this.grades = grades;
    }

    public asURL(): string {
        let ret = `${COURSE_URL}/courses/${COURSE_ID}/assignments/${ASSIGNMENT_ID}/submissions/${this.student.id}?`
        this.grades.forEach(grade => {
            ret += `rubric_assessment[${encodeURI(grade.rubricItem.rubricId)}][points]=${grade.grade}&rubric_assessment[${encodeURI(grade.rubricItem.rubricId)}][comments]=${encodeURI(grade.comment)}&`
        });
        return ret.substring(0, ret.length - 1);
    }

    public toString(): string {
        let ret = `${this.student.toString()}`;
        this.grades.forEach(grade => ret += `\n - ${grade.toString()}`);
        return ret;
    }
}